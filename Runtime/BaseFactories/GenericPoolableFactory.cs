using System;
using DataStructurePools.Interfaces;
using UnityEngine.Scripting;
using Zenject;

namespace DataStructurePools.BaseFactories
{
    public abstract class GenericPoolableFactory<T> : IFactory<T>
        where T : IPoolable<IPool<T>>, new()
    {
        private readonly IPool<T> _pool;

        [Preserve]
        protected GenericPoolableFactory(IPool<T> simplePool)
        {
            _pool = simplePool ?? throw new ArgumentNullException(nameof(simplePool));
        }

        public T Create()
        {
            return _pool.GetOrCreate(CreateInternal);
        }

        private T CreateInternal()
        {
            return new();
        }
    }
}