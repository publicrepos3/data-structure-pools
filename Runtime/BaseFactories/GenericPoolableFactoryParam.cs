using System;
using DataStructurePools.Interfaces;
using UnityEngine.Scripting;
using Zenject;

namespace DataStructurePools.BaseFactories
{
    public abstract class GenericPoolableFactoryParam<TP, T> : IFactory<TP, T>
        where T : IPoolable<IPool<TP, T>, TP>, new()
    {
        private readonly IPool<TP, T> _pool;

        [Preserve]
        protected GenericPoolableFactoryParam(IPool<TP, T> simplePool)
        {
            _pool = simplePool ?? throw new ArgumentNullException(nameof(simplePool));
        }

        public T Create(TP param)
        {
            return _pool.GetOrCreate(CreateInternal, param);
        }

        private T CreateInternal()
        {
            return new();
        }
    }
}