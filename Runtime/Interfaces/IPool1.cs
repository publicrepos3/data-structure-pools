using System;
using Zenject;

namespace DataStructurePools.Interfaces
{
    public interface IPool<in TP, T> : IPoolBase<T>
        where T : IPoolable<IPool<TP, T>, TP>
    {
        T GetOrCreate(Func<T> getData, TP parameter);
    }
}