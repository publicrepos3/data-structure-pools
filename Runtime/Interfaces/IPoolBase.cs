namespace DataStructurePools.Interfaces
{
    public interface IPoolBase<in T>
    {
        void Return(T data);
    }
}