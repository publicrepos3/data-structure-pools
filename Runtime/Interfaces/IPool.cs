using System;
using Zenject;

namespace DataStructurePools.Interfaces
{
    public interface IPool<T> : IPoolBase<T>
        where T : IPoolable<IPool<T>>
    {
        T GetOrCreate(Func<T> getData);
    }
}