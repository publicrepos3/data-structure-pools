using DataStructurePools.Implementation.DataStructure;
using DataStructurePools.Implementation.Pool;
using DataStructurePools.Interfaces;
using UnityEngine.Scripting;
using Zenject;

namespace DataStructurePools
{
    public class DataStructurePoolsInstaller : Installer<DataStructurePoolsInstaller>
    {
        [Preserve]
        public DataStructurePoolsInstaller()
        {
        }

        public override void InstallBindings()
        {
            Container
                .Bind(typeof(IPool<>))
                .To(typeof(SimplePool<>))
                .AsSingle();

            Container
                .Bind(typeof(IPool<,>))
                .To(typeof(SimplePool<,>))
                .AsSingle();

            Container
                .Bind(typeof(PoolableList<>.Factory))
                .AsSingle();

            Container
                .Bind(typeof(PoolableDictionary<,>.Factory))
                .AsSingle();

            Container
                .Bind(typeof(ComparablePoolableDictionary<,>.Factory))
                .AsSingle();

            Container
                .Bind(typeof(PoolableHashSet<>.Factory))
                .AsSingle();

            Container
                .Bind(typeof(ComparablePoolableHashSet<>.Factory))
                .AsSingle();
        }
    }
}