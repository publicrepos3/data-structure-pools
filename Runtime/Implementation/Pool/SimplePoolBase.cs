using System;
using System.Collections.Generic;
using DataStructurePools.Interfaces;
using UnityEngine.Scripting;

namespace DataStructurePools.Implementation.Pool
{
    internal abstract class SimplePoolBase<T> : IPoolBase<T>
    {
        private readonly Queue<T> _pool = new();

        [Preserve]
        internal SimplePoolBase()
        {
        }

        public virtual void Return(T data)
        {
            if (data == null)
                throw new ArgumentNullException(nameof(data));

            _pool.Enqueue(data);
        }

        protected T GetOrCreateInternal(Func<T> getData)
        {
            if (_pool.TryDequeue(out var result))
                return result;

            return getData();
        }
    }
}