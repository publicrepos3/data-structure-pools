using System;
using DataStructurePools.Interfaces;
using UnityEngine.Scripting;
using Zenject;

namespace DataStructurePools.Implementation.Pool
{
    internal class SimplePool<TP, T> : SimplePoolBase<T>, IPool<TP, T>
        where T : IPoolable<IPool<TP, T>, TP>
    {
        [Preserve]
        public SimplePool()
        {
        }

        public T GetOrCreate(Func<T> getData, TP parameter)
        {
            var result = GetOrCreateInternal(getData);
            result.OnSpawned(this, parameter);
            return result;
        }

        public override void Return(T data)
        {
            base.Return(data);
            data.OnDespawned();
        }
    }
}