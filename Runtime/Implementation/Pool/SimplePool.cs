using System;
using DataStructurePools.Interfaces;
using UnityEngine.Scripting;
using Zenject;

namespace DataStructurePools.Implementation.Pool
{
    internal class SimplePool<T> : SimplePoolBase<T>, IPool<T>
        where T : IPoolable<IPool<T>>
    {
        [Preserve]
        public SimplePool()
        {
        }

        public T GetOrCreate(Func<T> getData)
        {
            var result = GetOrCreateInternal(getData);
            result.OnSpawned(this);
            return result;
        }

        public override void Return(T data)
        {
            base.Return(data);
            data.OnDespawned();
        }
    }
}