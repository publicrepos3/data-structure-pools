#nullable enable
using System.Collections.Generic;

namespace DataStructurePools.Implementation.Common
{
    internal class InternalComparer<T> : IEqualityComparer<T>
    {
        public IEqualityComparer<T>? CurrentComparer;

        public bool Equals(T x, T y)
        {
            return CurrentComparer?.Equals(x, y) ?? object.Equals(x, y);
        }

        public int GetHashCode(T obj)
        {
            return CurrentComparer?.GetHashCode(obj) ?? obj?.GetHashCode() ?? 0;
        }
    }
}