using System;
using System.Collections.Generic;

namespace DataStructurePools.Implementation.Common
{
    public interface IPoolableDictionary<TKey, TValue> : IDictionary<TKey, TValue>, IDisposable
    {
        
    }
}