#nullable enable
using System;
using System.Collections.Generic;
using DataStructurePools.BaseFactories;
using DataStructurePools.Interfaces;
using UnityEngine.Scripting;
using Zenject;

namespace DataStructurePools.Implementation.DataStructure
{
    public class PoolableList<T> : List<T>, IPoolable<IPool<PoolableList<T>>>, IDisposable
    {
        private IPool<PoolableList<T>>? _pool;

        [Preserve]
        public PoolableList() : base()
        {
        }

        public void Dispose()
        {
            _pool?.Return(this);
        }

        void IPoolable<IPool<PoolableList<T>>>.OnDespawned()
        {
            Clear();
            _pool = null;
        }

        void IPoolable<IPool<PoolableList<T>>>.OnSpawned(IPool<PoolableList<T>> pool)
        {
            _pool = pool ?? throw new ArgumentNullException(nameof(pool));
        }

        public class Factory : GenericPoolableFactory<PoolableList<T>>
        {
            [Preserve]
            public Factory(IPool<PoolableList<T>> simplePool)
                : base(simplePool)
            {
            }
        }
    }
}