#nullable enable
using System;
using System.Collections.Generic;
using DataStructurePools.BaseFactories;
using DataStructurePools.Implementation.Common;
using DataStructurePools.Interfaces;
using UnityEngine.Scripting;
using Zenject;

namespace DataStructurePools.Implementation.DataStructure
{
    public class ComparablePoolableHashSet<T> :
        HashSet<T>,
        IPoolable<IPool<IEqualityComparer<T>, ComparablePoolableHashSet<T>>, IEqualityComparer<T>>,
        IDisposable
    {
        private readonly InternalComparer<T> _internalComparer;
        private IPool<IEqualityComparer<T>, ComparablePoolableHashSet<T>>? _pool;

        [Preserve]
        public ComparablePoolableHashSet()
            : this(new InternalComparer<T>())
        {
        }

        private ComparablePoolableHashSet(InternalComparer<T> internalComparer)
            : base(internalComparer)
        {
            _internalComparer = internalComparer ?? throw new ArgumentNullException(nameof(internalComparer));
        }

        public void Dispose()
        {
            _pool?.Return(this);
        }

        void IPoolable<IPool<IEqualityComparer<T>, ComparablePoolableHashSet<T>>, IEqualityComparer<T>>.OnDespawned()
        {
            Clear();
            _pool = null;
            _internalComparer.CurrentComparer = null;
        }

        void IPoolable<IPool<IEqualityComparer<T>, ComparablePoolableHashSet<T>>, IEqualityComparer<T>>.OnSpawned(
            IPool<IEqualityComparer<T>, ComparablePoolableHashSet<T>> pool,
            IEqualityComparer<T> comparer)
        {
            _pool = pool ?? throw new ArgumentNullException(nameof(pool));
            _internalComparer.CurrentComparer = comparer ?? throw new ArgumentNullException(nameof(comparer));
        }

        public class Factory : GenericPoolableFactoryParam<IEqualityComparer<T>,
            ComparablePoolableHashSet<T>>
        {
            [Preserve]
            public Factory(IPool<IEqualityComparer<T>, ComparablePoolableHashSet<T>> simplePool)
                : base(simplePool)
            {
            }
        }
    }
}