#nullable enable
using System;
using System.Collections.Generic;
using DataStructurePools.BaseFactories;
using DataStructurePools.Implementation.Common;
using DataStructurePools.Interfaces;
using UnityEngine.Scripting;
using Zenject;

namespace DataStructurePools.Implementation.DataStructure
{
    public class ComparablePoolableDictionary<TKey, TValue> :
        Dictionary<TKey, TValue>,
        IPoolableDictionary<TKey, TValue>,
        IPoolable<IPool<IEqualityComparer<TKey>, ComparablePoolableDictionary<TKey, TValue>>, IEqualityComparer<TKey>>
    {
        private readonly InternalComparer<TKey> _internalComparer;
        private IPool<IEqualityComparer<TKey>, ComparablePoolableDictionary<TKey, TValue>>? _pool;

        [Preserve]
        public ComparablePoolableDictionary()
            : this(new InternalComparer<TKey>())
        {
        }

        private ComparablePoolableDictionary(InternalComparer<TKey> internalComparer)
            : base(internalComparer)
        {
            _internalComparer = internalComparer ?? throw new ArgumentNullException(nameof(internalComparer));
        }

        public void Dispose()
        {
            _pool?.Return(this);
        }

        void IPoolable<IPool<IEqualityComparer<TKey>, ComparablePoolableDictionary<TKey, TValue>>,
            IEqualityComparer<TKey>>.OnDespawned()
        {
            Clear();
            _pool = null;
            _internalComparer.CurrentComparer = null;
        }

        void IPoolable<IPool<IEqualityComparer<TKey>, ComparablePoolableDictionary<TKey, TValue>>,
            IEqualityComparer<TKey>>.OnSpawned(
            IPool<IEqualityComparer<TKey>, ComparablePoolableDictionary<TKey, TValue>> pool,
            IEqualityComparer<TKey> comparer)
        {
            _pool = pool ?? throw new ArgumentNullException(nameof(pool));
            _internalComparer.CurrentComparer = comparer ?? throw new ArgumentNullException(nameof(comparer));
        }

        public class Factory : GenericPoolableFactoryParam<IEqualityComparer<TKey>,
            ComparablePoolableDictionary<TKey, TValue>>
        {
            [Preserve]
            public Factory(IPool<IEqualityComparer<TKey>, ComparablePoolableDictionary<TKey, TValue>> simplePool)
                : base(simplePool)
            {
            }
        }
    }
}