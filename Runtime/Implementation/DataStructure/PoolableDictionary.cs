#nullable enable
using System;
using System.Collections.Generic;
using DataStructurePools.BaseFactories;
using DataStructurePools.Implementation.Common;
using DataStructurePools.Interfaces;
using UnityEngine.Scripting;
using Zenject;

namespace DataStructurePools.Implementation.DataStructure
{
    public class PoolableDictionary<TKey, TValue> : 
        Dictionary<TKey, TValue>,
        IPoolableDictionary<TKey, TValue>,
        IPoolable<IPool<PoolableDictionary<TKey, TValue>>>
    {
        private IPool<PoolableDictionary<TKey, TValue>>? _pool;

        [Preserve]
        public PoolableDictionary() : base()
        {
        }

        public void Dispose()
        {
            _pool?.Return(this);
        }

        void IPoolable<IPool<PoolableDictionary<TKey, TValue>>>.OnDespawned()
        {
            Clear();
            _pool = null;
        }

        void IPoolable<IPool<PoolableDictionary<TKey, TValue>>>.OnSpawned(IPool<PoolableDictionary<TKey, TValue>> pool)
        {
            _pool = pool ?? throw new ArgumentNullException(nameof(pool));
        }

        public class Factory : GenericPoolableFactory<PoolableDictionary<TKey, TValue>>
        {
            [Preserve]
            public Factory(IPool<PoolableDictionary<TKey, TValue>> simplePool)
                : base(simplePool)
            {
            }
        }
    }
}