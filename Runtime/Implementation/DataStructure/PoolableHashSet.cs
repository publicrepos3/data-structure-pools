#nullable enable
using System;
using System.Collections.Generic;
using DataStructurePools.BaseFactories;
using DataStructurePools.Interfaces;
using UnityEngine.Scripting;
using Zenject;

namespace DataStructurePools.Implementation.DataStructure
{
    public class PoolableHashSet<T> : HashSet<T>, IPoolable<IPool<PoolableHashSet<T>>>, IDisposable
    {
        private IPool<PoolableHashSet<T>>? _pool;

        [Preserve]
        public PoolableHashSet() : base()
        {
        }

        public void Dispose()
        {
            _pool?.Return(this);
        }

        void IPoolable<IPool<PoolableHashSet<T>>>.OnDespawned()
        {
            Clear();
            _pool = null;
        }

        void IPoolable<IPool<PoolableHashSet<T>>>.OnSpawned(IPool<PoolableHashSet<T>> pool)
        {
            _pool = pool ?? throw new ArgumentNullException(nameof(pool));
        }

        public class Factory : GenericPoolableFactory<PoolableHashSet<T>>
        {
            [Preserve]
            public Factory(IPool<PoolableHashSet<T>> simplePool)
                : base(simplePool)
            {
            }
        }
    }
}